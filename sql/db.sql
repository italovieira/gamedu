CREATE DATABASE app;
USE app;

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(40) NOT NULL,
    login VARCHAR(20) NOT NULL,
    password VARCHAR(255) NOT NULL,
    photo VARCHAR(255),
    age TINYINT UNSIGNED NOT NULL,
    user_type_id INT NOT NULL
);

CREATE TABLE user_types (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(13) NOT NULL
);

CREATE TABLE courses (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(30) NOT NULL
);

CREATE TABLE registries (
    id INT AUTO_INCREMENT PRIMARY KEY,
    course_id INT,
    user_id INT,
    CONSTRAINT fk_registries_courses  FOREIGN KEY (course_id) REFERENCES courses(id),
    CONSTRAINT fk_registries_users  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE lessons (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(13) NOT NULL,
    text TEXT,
    video VARCHAR(255), # upload
    document VARCHAR(255), # upload
    links VARCHAR(2000),
    date TIMESTAMP, # trigger
    course_id INT,
    user_id INT,
    CONSTRAINT fk_lessons_courses  FOREIGN KEY (course_id) REFERENCES courses(id),
    CONSTRAINT fk_lessons_users  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE logs (
    id INT AUTO_INCREMENT PRIMARY KEY,
    moment TIMESTAMP NOT NULL,
    user_id INT,
    lesson_id INT,
    CONSTRAINT fk_logs_users FOREIGN KEY (user_id) REFERENCES users(id),
    CONSTRAINT fk_logs_lessons FOREIGN KEY (lesson_id) REFERENCES lessons(id)
);

CREATE TABLE comments (
    id INT AUTO_INCREMENT PRIMARY KEY,
    date TIMESTAMP,
    comment VARCHAR(2000),
    user_id INT,
    lesson_id INT,
    CONSTRAINT fk_comments_users FOREIGN KEY (user_id) REFERENCES users(id),
    CONSTRAINT fk_comments_lessons FOREIGN KEY (lesson_id) REFERENCES lessons(id)
);
