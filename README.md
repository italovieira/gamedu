Sistema para a bolsa de Programador WEB (PHP e MySQL) do Gamedu (IMD), com o framework CakePHP.

O script do banco de dados está em 'sql/db.sql'.

Renomear arquivo 'config.default.php' para 'config.php' e definir a configuração de acesso ao banco de dados.

Página principal da aplicação: '/pages/main'.
