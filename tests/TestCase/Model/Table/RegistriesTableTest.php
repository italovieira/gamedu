<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RegistriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RegistriesTable Test Case
 */
class RegistriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\RegistriesTable
     */
    public $Registries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.registries',
        'app.courses',
        'app.lessons',
        'app.comments',
        'app.logs',
        'app.users',
        'app.user_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Registries') ? [] : ['className' => 'App\Model\Table\RegistriesTable'];
        $this->Registries = TableRegistry::get('Registries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Registries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
