<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Lesson Entity
 *
 * @property int $id
 * @property string $name
 * @property string $attachment
 * @property string $links
 * @property \Cake\I18n\Time $scheduling
 * @property int $course_id
 *
 * @property \App\Model\Entity\Course $course
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Log[] $logs
 */
class Lesson extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
