<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Ir à página inicial'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Voltar para o curso'), ['controller' => 'Courses', 'action' => 'view', $lesson->course_id]) ?> </li>
    </ul>
</nav>
<div class="lessons view large-9 medium-8 columns content">
    <h3><?= h($lesson->name) ?></h3>
    <h4><?= h($lesson->user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Documento') ?></th>
            <td><?= $this->Html->link('../files/Lessons/document/' .  '/' . $lesson->document) ?></td>
        </tr>
        <tr>
            <th><?= __('Vídeo') ?></th>
            <td><?= $this->Html->link('../files/Lessons/video/' .  '/' . $lesson->video) ?></td>
        </tr>
        <tr>
            <th><?= __('Links') ?></th>
            <td><?= h($lesson->links) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Fórum de discussão') ?></h4>
        <?php if (!empty($lesson->comments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Aluno') ?></th>
                <th><?= __('Comentário') ?></th>
                <th><?= __('Data/hora') ?></th>
            </tr>
            <?php foreach ($lesson->comments as $comments): ?>
            <tr>
                <td><?= h($comments->user->name) ?></td>
                <td><?= h($comments->comment) ?></td>
                <td><?= h($comments->date) ?></td>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
