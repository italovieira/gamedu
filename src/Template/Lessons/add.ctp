<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Ir à página inicial'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Voltar para o curso'), ['controller' => 'Courses', 'action' => 'view', $this->request->pass[0]]) ?></li>
    </ul>
</nav>
<div class="lessons form large-9 medium-8 columns content">
    <?= $this->Form->create($lesson, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Adicionar aula') ?></legend>
        <?php
            echo $this->Form->input('name', ['label' => 'Título']);
            echo $this->Form->input('text', ['label' => 'Conteúdo']);
            echo $this->Form->input('document', ['type' => 'file']);
            echo $this->Form->input('video', ['type' => 'file']);
            echo $this->Form->input('links', ['type' => 'textarea', 'label' => 'Lista de links']);
            echo $this->Form->input('date', ['label' => 'Data/hora de agendamento']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
</div>
