<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Ir à página inicial'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Voltar para o curso'), ['controller' => 'Courses', 'action' => 'view', $this->request->pass[0]]) ?></li>
    </ul>
</nav>
<div class="registries form large-9 medium-8 columns content">
    <?= $this->Form->create($registry) ?>
    <fieldset>
        <legend><?= __('Adicionar professor') ?></legend>
        <?php
            echo $this->Form->input('user_id', ['label' => 'Professor', 'options' => $users, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Ok')) ?>
    <?= $this->Form->end() ?>
</div>
