<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Voltar à página inicial'), ['controller' => 'Users', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="registries form large-9 medium-8 columns content">
    <?= $this->Form->create($registry) ?>
    <fieldset>
        <legend><?= __('Matrícula em novo curso') ?></legend>
        <?php
            echo $this->Form->input('course_id', ['label' => 'Curso', 'options' => $courses, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
