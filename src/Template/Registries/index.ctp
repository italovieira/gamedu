<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Registry'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Courses'), ['controller' => 'Courses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Course'), ['controller' => 'Courses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="registries index large-9 medium-8 columns content">
    <h3><?= __('Registries') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('course_id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($registries as $registry): ?>
            <tr>
                <td><?= $this->Number->format($registry->id) ?></td>
                <td><?= $registry->has('course') ? $this->Html->link($registry->course->name, ['controller' => 'Courses', 'action' => 'view', $registry->course->id]) : '' ?></td>
                <td><?= $registry->has('user') ? $this->Html->link($registry->user->name, ['controller' => 'Users', 'action' => 'view', $registry->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $registry->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $registry->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $registry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $registry->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
