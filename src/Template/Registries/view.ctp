<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Registry'), ['action' => 'edit', $registry->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Registry'), ['action' => 'delete', $registry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $registry->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Registries'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Registry'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Courses'), ['controller' => 'Courses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Course'), ['controller' => 'Courses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="registries view large-9 medium-8 columns content">
    <h3><?= h($registry->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Course') ?></th>
            <td><?= $registry->has('course') ? $this->Html->link($registry->course->name, ['controller' => 'Courses', 'action' => 'view', $registry->course->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $registry->has('user') ? $this->Html->link($registry->user->name, ['controller' => 'Users', 'action' => 'view', $registry->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($registry->id) ?></td>
        </tr>
    </table>
</div>
