<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Voltar à página inicial'), ['controller' => 'Users', 'action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Editar suas informações') ?></legend>
        <?php
            echo $this->Form->input('name', ['label' => 'Nome']);
            echo $this->Form->input('login');
            echo $this->Form->input('password', ['label' => 'Nova senha']);
            echo $this->Form->input('age', ['label' => 'Idade']);
            echo $this->Form->input('photo', ['label' => 'Imagem', 'type' => 'file']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Salvar')) ?>
    <?= $this->Form->end() ?>
</div>
