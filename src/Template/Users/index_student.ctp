<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Matricular-se em curso'), ['controller' => 'Registries', 'action' => 'addStudent']) ?> </li>
        <li><?= $this->Html->link(__('Editar suas informações'), ['controller' => 'Users', 'action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Html->link(__('Sair'), ['controller' => 'Users', 'action' => 'logout']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <div class="related">
        <h4><?= __('Seus cursos') ?></h4>
        <?php if (!empty($user->courses)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Nome') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($user->courses as $courses): ?>
            <tr>
                <td><?= h($courses->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Courses', 'action' => 'view', $courses->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
