<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Ir à página inicial'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Voltar ao curso'), ['controller' => 'Courses', 'action' => 'view', $this->request->pass['1']]) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <td><?= $this->Html->image('../files/Users/photo/' .  '/' . $user->photo) ?></td>
        </tr>
        <tr>
            <th><?= __('Função') ?></th>
            <td><?= h($user->user_type->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Idade') ?></th>
            <td><?= $this->Number->format($user->age) ?></td>
        </tr>
    </table>
</div>
