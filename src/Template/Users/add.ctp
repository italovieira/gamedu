<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Login'), ['controller' => 'Registries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Voltar'), ['controller' => 'Registries', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user, ['type' => 'file']) ?>
    <fieldset>
        <legend><?= __('Adicionar usuário') ?></legend>
        <?php
            echo $this->Form->input('name', ['label' => 'Nome']);
            echo $this->Form->input('login', ['label' => 'Nome de usuário']);
            echo $this->Form->input('password', ['label' => 'Senha']);
            echo $this->Form->input('age',  ['label' => 'Idade']);
            echo $this->Form->input('photo', ['type' => 'file', 'label' => 'Foto']);
            echo $this->Form->input('user_type_id', ['options' => $userTypes, 'label' => 'Tipo de usuário']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
