<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Ir à página inicial'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div class="courses view large-9 medium-8 columns content">
    <h3><?= h($course->name) ?></h3>
    <div class="related">
        <h4><?= __('Aulas') ?></h4>
        <?php if (!empty($course->lessons)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Nome') ?></th>
                <th><?= __('Criador') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($course->lessons as $lessons): ?>
            <tr>
                <td><?= h($lessons->name) ?></td>
                <td><?= $this->Html->link($lessons->user->name, ['controller' => 'Users', 'action' => 'view', $lessons->user->id, $course->id]) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Realizar aula'), ['controller' => 'Lessons', 'action' => 'view', $lessons->id, $course->id, isset($lessons->logs[0]->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>

    <div class="related">
        <h4><?= __('Professores') ?></h4>
        <?php if (!empty($teachers->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Nome') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($teachers->users as $teacher): ?>
            <tr>
                <td><?= h($teacher->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Users', 'action' => 'view', $teacher->id, $teachers->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>

    <div class="related">
        <h4><?= __('Outros alunos') ?></h4>
        <?php if (!empty($students->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Nome') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($students->users as $student): ?>
            <tr>
                <td><?= h($student->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Users', 'action' => 'view', $student->id, $students->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>

    <div class="related">
        <h4><?= __('Histórico de aulas') ?></h4>
        <?php if (!empty($course->lessons)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Aula') ?></th>
                <th><?= __('Data') ?></th>
            </tr>
            <?php foreach ($course->lessons as $lessons): ?>
            <?php foreach ($lessons->logs as $logs): ?>
            <tr>
                <td><?= h($lessons->name) ?></td>
                <td><?= h($logs->moment) ?></td>
            </tr>
            <?php endforeach; ?>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
