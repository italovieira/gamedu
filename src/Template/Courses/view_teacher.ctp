<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Ir à página inicial'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Adicionar aula'), ['controller' => 'Lessons', 'action' => 'add', $course->id]) ?> </li>
    </ul>
</nav>
<div class="courses view large-9 medium-8 columns content">
    <h3><?= h($course->name) ?></h3>
    <div class="related">
        <h4><?= __('Aulas') ?></h4>
        <?php if (!empty($course->lessons)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('#') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Data') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($course->lessons as $lessons): ?>
            <tr>
                <td><?= h($lessons->id) ?></td>
                <td><?= h($lessons->name) ?></td>
                <td><?= h($lessons->date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Lessons', 'action' => 'view', $lessons->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Lessons', 'action' => 'edit', $lessons->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>

    <div class="related">
        <h4><?= __('Outros professores') ?></h4>
        <?php if (!empty($teachers->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('#') ?></th>
                <th><?= __('Nome') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($teachers->users as $teacher): ?>
            <tr>
                <td><?= h($teacher->id) ?></td>
                <td><?= h($teacher->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Users', 'action' => 'view', $teacher->id, $teachers->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>

    <div class="related">
        <h4><?= __('Alunos') ?></h4>
        <?php if (!empty($students->users)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('#') ?></th>
                <th><?= __('Nome') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
            <?php foreach ($students->users as $student): ?>
            <tr>
                <td><?= h($student->id) ?></td>
                <td><?= h($student->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Users', 'action' => 'view', $student->id, $course->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>

</div>
