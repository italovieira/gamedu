<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Courses Controller
 *
 * @property \App\Model\Table\CoursesTable $Courses
 */
class CoursesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $courses = $this->Courses->find('all')
            ->matching('Users', function ($q) {
                return $q->where(['Users.id' => $this->Auth->user('id')]);
            });

        $this->set('courses', $this->paginate($courses));
        $this->set('user_id', $this->Auth->user('id'));
        $this->set('_serialize', ['courses']);

        if ($this->Auth->user('user_type_id') === 1) {
            $this->render('index_admin');
        } else if ($this->Auth->user('user_type_id') === 2) {
            $this->render('index_teacher');
        } else if ($this->Auth->user('user_type_id') === 3) {
            return $this->redirect(['controller' => 'Users', 'action' => 'index']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Course id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        //Obtém aulas e professores de tal curso
        $course = $this->Courses->get($id, [
            'contain' => [
                'Lessons',
                'Lessons.Users' => function ($q) {
                    return $q->select(['id', 'name']);
                },
                'Lessons.Logs' => function ($q) {
                    return $q ->where(['Logs.user_id' => $this->Auth->user('id')]);
                }
        ]
        ]);

        $students = $this->Courses->get($id, [
            'contain' => ['Users' => function ($q) {
                return $q
                    ->where(['Users.user_type_id' => 3])
                    ->where(['Users.id !=' => $this->Auth->user('id')]);
            }]
        ]);

        $teachers = $this->Courses->get($id, [
            'contain' => ['Users' => function ($q) {
                return $q
                    ->where(['Users.user_type_id' => 2])
                    ->where(['Users.id !=' => $this->Auth->user('id')]);
            }]
        ]);


        $this->set(compact('course', 'students', 'teachers'));
        $this->set('_serialize', ['course']);

        $user_type_id = $this->Auth->user('user_type_id');
        if ($user_type_id === 1) {
            $this->render('viewAdmin');            
        } else if ($user_type_id === 2) {
            $this->render('viewTeacher');            
        } else if ($user_type_id === 3) {
            $this->render('viewStudent');            
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $course = $this->Courses->newEntity();
        if ($this->request->is('post')) {
            $course = $this->Courses->patchEntity($course, $this->request->data);
            if ($this->Courses->save($course)) {

                // Adicionar vínculo do administrador com o curso criado
                $registriesTable = TableRegistry::get('Registries');
                $registry = $registriesTable->newEntity();

                $registry->course_id = $course->id;
                $registry->user_id = $this->Auth->user('id');

                if ($registriesTable->save($registry)) {
                    $id = $registry->id;
                }

                $this->Flash->success(__('O curso foi criado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O curso não pôde ser criado. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('course'));
        $this->set('_serialize', ['course']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Course id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $course = $this->Courses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $course = $this->Courses->patchEntity($course, $this->request->data);
            if ($this->Courses->save($course)) {
                $this->Flash->success(__('The course has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The course could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('course'));
        $this->set('_serialize', ['course']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Course id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $course = $this->Courses->get($id);
        if ($this->Courses->delete($course)) {
            $this->Flash->success(__('The course has been deleted.'));
        } else {
            $this->Flash->error(__('The course could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
