<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Lessons Controller
 *
 * @property \App\Model\Table\LessonsTable $Lessons
 */
class LessonsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Courses']
        ];
        $lessons = $this->paginate($this->Lessons);

        $this->set(compact('lessons'));
        $this->set('_serialize', ['lessons']);
    }

    /**
     * View method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Adicionar registro de aula realizada por aluno
        if ($this->Auth->user('user_type_id') === 3 && !isset($this->request->pass[2])) {
            $logsTable = TableRegistry::get('Logs');
            $log = $logsTable->newEntity();

            $log->lesson_id = $id;
            $log->user_id = $this->Auth->user('id');

            if ($logsTable->save($log)) {
                //$id = $log->id;
            }
        }

        $lesson = $this->Lessons->get($id, [
            'contain' => ['Courses', 'Comments', 'Comments.Users', 'Users' => function ($q) {
                return $q->select(['name']);
            }]
        ]);

        $this->set('lesson', $lesson);
        $this->set('_serialize', ['lesson']);

        if ($this->Auth->user('user_type_id') === 3) {
            $this->render('viewStudent');
        }
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($course_id = null)
    {
        $lesson = $this->Lessons->newEntity();
        if ($this->request->is('post')) {
            $lesson = $this->Lessons->patchEntity($lesson, $this->request->data);
            $lesson->course_id = $course_id;
            $lesson->user_id = $this->Auth->user('id');
            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('A aula foi salva com sucesso.'));

                return $this->redirect(['controller' => 'Courses', 'action' => 'view', $course_id]);
            } else {
                $this->Flash->error(__('A aula não pôde ser salva. Por favor, tente novamente.'));
            }
        }
        $courses = $this->Lessons->Courses->find('list', ['limit' => 200]);
        $this->set(compact('lesson', 'courses'));
        $this->set('_serialize', ['lesson']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lesson = $this->Lessons->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lesson = $this->Lessons->patchEntity($lesson, $this->request->data);
            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('The lesson has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The lesson could not be saved. Please, try again.'));
            }
        }
        $courses = $this->Lessons->Courses->find('list', ['limit' => 200]);
        $this->set(compact('lesson', 'courses'));
        $this->set('_serialize', ['lesson']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lesson = $this->Lessons->get($id);
        if ($this->Lessons->delete($lesson)) {
            $this->Flash->success(__('The lesson has been deleted.'));
        } else {
            $this->Flash->error(__('The lesson could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
