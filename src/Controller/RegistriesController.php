<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Registries Controller
 *
 * @property \App\Model\Table\RegistriesTable $Registries
 */
class RegistriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Courses', 'Users']
        ];
        $registries = $this->paginate($this->Registries);

        $this->set(compact('registries'));
        $this->set('_serialize', ['registries']);
    }

    /**
     * View method
     *
     * @param string|null $id Registry id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $registry = $this->Registries->get($id, [
            'contain' => ['Courses', 'Users']
        ]);

        $this->set('registry', $registry);
        $this->set('_serialize', ['registry']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $registry = $this->Registries->newEntity();
        if ($this->request->is('post')) {
            $registry = $this->Registries->patchEntity($registry, $this->request->data);
            if ($this->Registries->save($registry)) {
                $this->Flash->success(__('The registry has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The registry could not be saved. Please, try again.'));
            }
        }
        $courses = $this->Registries->Courses->find('list', ['limit' => 200]);
        $users = $this->Registries->Users->find('list', ['limit' => 200]);
        $this->set(compact('registry', 'courses', 'users'));
        $this->set('_serialize', ['registry']);
    }

    //Adiciona professores aos cursos
    public function addTeacher($course_id = null)
    {
        $registry = $this->Registries->newEntity();
        if ($this->request->is('post')) {
            $registry = $this->Registries->patchEntity($registry, $this->request->data);
            $registry->course_id = $course_id;
            if ($this->Registries->save($registry)) {
                $this->Flash->success(__('O professor foi adicionado na aula com sucesso.'));

                return $this->redirect(['controller' => 'Courses', 'action' => 'view', $course_id]);
            } else {
                $this->Flash->error(__('The registry could not be saved. Please, try again.'));
            }
        }
        $courses = $this->Registries->Courses->find('list', ['limit' => 200]);

        $users = $this->Registries->Users->find('list', ['limit' => 200])
            ->where(['Users.user_type_id' => 2])
            ->notMatching('Courses', function ($q) {
                return $q->where(['Courses.id' => $this->request->pass[0]]);
            });

        $this->set(compact('registry', 'courses', 'users'));
        $this->set('_serialize', ['registry']);
    }

    //Função que matricula alunos aos cursos
    public function addStudent()
    {
        $registry = $this->Registries->newEntity();
        if ($this->request->is('post')) {
            $registry = $this->Registries->patchEntity($registry, $this->request->data);
            $registry->user_id = $this->Auth->user('id');
            if ($this->Registries->save($registry)) {
                $this->Flash->success(__('Matrícula realizada com sucesso.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'index']);
            } else {
                $this->Flash->error(__('The registry could not be saved. Please, try again.'));
            }
        }

        $courses = $this->Registries->Courses->find('list', ['limit' => 200])
            ->notMatching('Users', function ($q) {
                return $q->where(['Users.id' => $this->Auth->user('id')]);
            });

        $this->set(compact('registry', 'courses', 'users'));
        $this->set('_serialize', ['registry']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Registry id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $registry = $this->Registries->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $registry = $this->Registries->patchEntity($registry, $this->request->data);
            if ($this->Registries->save($registry)) {
                $this->Flash->success(__('The registry has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The registry could not be saved. Please, try again.'));
            }
        }
        $courses = $this->Registries->Courses->find('list', ['limit' => 200]);
        $users = $this->Registries->Users->find('list', ['limit' => 200]);
        $this->set(compact('registry', 'courses', 'users'));
        $this->set('_serialize', ['registry']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Registry id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $registry = $this->Registries->get($id);
        if ($this->Registries->delete($registry)) {
            $this->Flash->success(__('Professor excluído deste curso com sucesso.'));
        } else {
            $this->Flash->error(__('Erro ao tentar excluir professor do curso. Por favor, tente novamente.'));
        }
        return $this->redirect($this->referer());
    }
}
